FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster-arm32v7 AS builder

WORKDIR /src

COPY PiThermostat.csproj /src/PiThermostat.csproj

RUN dotnet restore PiThermostat.csproj

COPY . /src/

RUN dotnet publish -c Release -o /build --no-restore PiThermostat.csproj

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim-arm32v7

LABEL maintainer "Clinton Lee <clintonalee@gmail.com>"

WORKDIR /app

ENTRYPOINT [ "dotnet", "PiThermostat.dll" ]

COPY --from=builder /build /app
