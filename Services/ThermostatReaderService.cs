﻿using System;
using System.Device.Gpio;
using System.Threading;
using System.Threading.Tasks;
using Iot.Device.DHTxx;
using Microsoft.Extensions.Hosting;

namespace PiThermostat
{
    public class ThermostatReaderService : IHostedService
    {
        private readonly StatusHolder statusHolder;
        private CancellationTokenSource cancellationTokenSource;
        private Task loop;

        public ThermostatReaderService(StatusHolder statusHolder)
            => this.statusHolder = statusHolder;

        public Task StartAsync(CancellationToken cancellationToken)
        {
            cancellationTokenSource = new CancellationTokenSource();
            var ct = cancellationTokenSource.Token;
            var canceledTaskCompletionSource = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);
            var cancelTask = canceledTaskCompletionSource.Task;
            ct.Register(() => canceledTaskCompletionSource.TrySetCanceled());
            loop = Task.Run(async () =>
            {
                try
                {
                    using var controller = new GpioController();
                    using var dht = new Dht11(int.Parse(Environment.GetEnvironmentVariable("GPIO") ?? "4"), PinNumberingScheme.Logical);
                    while (true)
                    {
                        ct.ThrowIfCancellationRequested();
                        bool success = true;
                        var temp = dht.Temperature.Celsius;
                        var humidity = dht.Humidity;
                        if (double.IsNaN(temp))
                        {
                            success = false;
                        }
                        else
                        {
                            statusHolder.SetTemperature(temp);
                        }
                        if (double.IsNaN(humidity))
                        {
                            success = false;
                        }
                        else
                        {
                            statusHolder.SetHumidity(humidity);
                        }
                        if(success)
                        {
                            Console.WriteLine($"{DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:ssZ")} temperature: {temp}c, humidity: {humidity}%");
                        }
                        await Task.WhenAny(Task.Delay(success ? TimeSpan.FromMinutes(5) : TimeSpan.FromMilliseconds(100)), cancelTask);
                    }
                }
                catch(OperationCanceledException)
                {
                    return;
                }
                catch(Exception ex)
                {
                    Console.WriteLine($"Unexpected exception: {ex}");
                    Thread.Sleep(5000);
                    Environment.Exit(1);
                }
            }, cancellationToken);
            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            cancellationTokenSource.Cancel();
            await loop;
        }
    }
}
