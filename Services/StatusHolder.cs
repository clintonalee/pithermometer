﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PiThermostat
{
    public class StatusHolder
    {
        private (double celcius, DateTime stamp)? temperature;
        private (double humidity, DateTime stamp)? humidity;

        private readonly List<TaskCompletionSource<double>> humidityTasks = new List<TaskCompletionSource<double>>();
        private readonly List<TaskCompletionSource<double>> temperatureTasks = new List<TaskCompletionSource<double>>();


        public void SetTemperature(double celcius)
        {
            DateTime utcNow = DateTime.UtcNow;

            lock (temperatureTasks)
            {
                temperature = (celcius, utcNow);
                foreach(var t in temperatureTasks)
                {
                    t.TrySetResult(celcius);
                }
                temperatureTasks.Clear();
            }
        }

        public void SetHumidity(double humidity)
        {
            DateTime utcNow = DateTime.UtcNow;

            lock (humidityTasks)
            {
                this.humidity = (humidity, utcNow);
                foreach (var t in humidityTasks)
                {
                    t.TrySetResult(humidity);
                }
                humidityTasks.Clear();
            }
        }


        public Task<double> GetTemperature()
        {
            lock(temperatureTasks)
            {
                if (temperature is (var tempC, _))
                {
                    return Task.FromResult(tempC);
                }
                else
                {
                    var tcs = new TaskCompletionSource<double>(TaskCreationOptions.RunContinuationsAsynchronously);
                    temperatureTasks.Add(tcs);
                    return tcs.Task;
                }
            }
        }


        public Task<double> GetHumidity()
        {
            lock (humidityTasks)
            {
                if (humidity is (var h, _))
                {
                    return Task.FromResult(h);
                }
                else
                {
                    var tcs = new TaskCompletionSource<double>(TaskCreationOptions.RunContinuationsAsynchronously);
                    temperatureTasks.Add(tcs);
                    return tcs.Task;
                }
            }
        }
    }
}
