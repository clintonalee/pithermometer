﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PiThermostat.Controllers
{
    [Route("[controller]")]
    public class StatusController : Controller
    {
        private StatusHolder statusHolder;

        public StatusController(StatusHolder statusHolder) => this.statusHolder = statusHolder;

        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            return Ok(new
            {
                currentTemperature = await statusHolder.GetTemperature(),
                humidity = await statusHolder.GetHumidity()
            });
        }
    }
}
